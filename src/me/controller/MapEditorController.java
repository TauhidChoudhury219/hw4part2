/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.controller;

import me.MapViewerApp;
import me.gui.NewMapDialog;
import me.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author Tchou808
 */
public class MapEditorController {
    
     AppTemplate app;
    
    public MapEditorController(AppTemplate initApp) {
	app = initApp;
    }

    
    public void processNewMap() {	
	// ENABLE/DISABLE THE PROPER BUTTONS 
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	//workspace.reloadWorkspace();
	NewMapDialog dialog= new NewMapDialog();
	app.getGUI().updateToolbarControls(false);
    }
    
    
    
    
    
}
