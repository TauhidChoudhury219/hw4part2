/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import me.data.DataManager;

import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import me.gui.Workspace;
/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    
static final String JSON_SUBREGIONS = "SUBREGIONS";
static final String JSON_SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
static final String JSON_NUMBER_OF_SUBREGIONS ="NUMBER_OF_SUBREGION_POLYGONS";


static final String JSON_COORDINATE_PATH="coordinate path";
static final String JSON_REGION_NAME = "region name";
static final String JSON_BORDER_COLOR = "border color";
static final String JSON_BACKGROUND_COLOR = "background color";

static final String JSON_BORDER_THICKNESS="border thickness";
static final String JSON_MAP_ZOOM="map zoom";
static final String JSON_MAP_WIDTH="map width";
static final String JSON_MAP_HEIGHT="map height";
static final String JSON_MAP_FLAG = "flag path";
static final String JSON_MAP_SEAL = "seal path";

static final String JSON_SUBREGION_NAME = "name";
static final String JSON_RED = "red";
static final String JSON_BLUE = "blue";
static final String JSON_GREEN = "green";
static final String JSON_LEADER = "leader";
static final String JSON_CAPITAL = "capital";
static final String JSON_LEADER_IMAGE = "leader image";
static final String JSON_FLAG_IMAGE = "flag image";

static final String JSON_SUBARRAY= "";

public Polygon polygon;
public ArrayList<Polygon> coordinates= new ArrayList();
public ArrayList<Polygon> subregionPolygons=new ArrayList();



@Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	
	
	// GET THE DATA
	DataManager dataManager = (DataManager)data;
	
	// Image paths
        String sealpath = dataManager.getSealPath();
	String flagpath=dataManager.getFlagPath();
	
	//Map edit items
	String mapName = dataManager.getMapName();
        String backgroundcolor= dataManager.getBackgroundColor();
	String bordercolor= dataManager.getBorderColor();
	String mapFilePath= dataManager.getMapFilePath();
	double borderThickness= dataManager.getBorderThickness();
	double mapwidth = dataManager.getMapWidth();
	double mapheight= dataManager.getMapHeight();
	double mapzoom= dataManager.getMapZoom();
	
	//SUBREGION DATA
	String leader=dataManager.getLeader();
	String capital=dataManager.getCapital();
	String subname=dataManager.getSubName();
	String subflagpath=dataManager.getFlagImage();
	String leaderpath=dataManager.getLeaderImage();
	int red=dataManager.getRed();
	int blue=dataManager.getBlue();
	int green=dataManager.getGreen();
	
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		ArrayList<Polygon> subregions = dataManager.getSubregionPolygons();
		for(int i=0; i <subregions.size();i++){
		 JsonObject itemJson = Json.createObjectBuilder()
	
		.add(JSON_SUBREGION_NAME, subregions)
		.add(JSON_LEADER, leader)
		.add(JSON_CAPITAL, capital)
		.add(JSON_RED, red)
		.add(JSON_BLUE, blue)
		.add(JSON_GREEN, green)
		  arrayBuilder.add(itemJson);
		  
			}
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_REGION_NAME, mapName)
		.add(JSON_COORDINATE_PATH, mapFilePath)
		.add(JSON_BACKGROUND_COLOR, backgroundcolor)
		.add(JSON_BORDER_COLOR, bordercolor)
		.add(JSON_MAP_ZOOM, mapzoom)
		.add(JSON_MAP_WIDTH, mapwidth)
		.add(JSON_MAP_HEIGHT, mapheight)
		.add(JSON_BORDER_THICKNESS, borderThickness)
		.add(JSON_MAP_SEAL, subregions)
		.add(JSON_MAP_FLAG,flagpath)
		
			}
//		.add(JSON_SUBARRAY,)
		
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
	
      



	
	



    

    public void ediwnloadData(AppDataComponent data, String filePath) throws IOException {

	
	// GET THE DATA
	DataManager dataManager = (DataManager)data;
	dataManager.getSubregionPolygons().clear();
	dataManager.getImageItems().clear();
	dataManager.getCoordinates().clear();
	// Image paths
        String sealpath = dataManager.getSealPath();
	String flagpath=dataManager.getFlagPath();
	
	//Map edit items
	String mapName = dataManager.getMapName();
        String backgroundcolor= dataManager.getBackgroundColor();
	String bordercolor= dataManager.getBorderColor();
	String mapFilePath= dataManager.getMapFilePath();
	double borderThickness= dataManager.getBorderThickness();
	double mapwidth = dataManager.getMapWidth();
	double mapheight= dataManager.getMapHeight();
	double mapzoom= dataManager.getMapZoom();
	
	//SUBREGION DATA
	String leader=dataManager.getLeader();
	String capital=dataManager.getCapital();
	String subname=dataManager.getSubName();
	String subflagpath=dataManager.getFlagImage();
	String leaderpath=dataManager.getLeaderImage();
	int red=dataManager.getRed();
	int blue=dataManager.getBlue();
	int green=dataManager.getGreen();
	
	
	
	//loadMap(data, mapFilePath);
	
	
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	JsonObject json = loadJSONFile(filePath);	
	JsonArray subArray1 = json.getJsonArray(JSON_NUMBER_OF_SUBREGIONS);
	for (int i = 0; i < subArray1.size(); i++) { 
	    
	
	// NOW BUILD THE JSON ARRAY FOR THE SubRegions
	
		JsonArray subRegionPolygons = json.getJsonArray(JSON_SUBREGION_POLYGONS);
	 	    
	 
		for(int v=0; v<subRegionPolygons.size(); v++){
		  
		    JsonArray coordinateArray= subRegionPolygons.getJsonArray(v);
		    for (int r=0; r<coordinateArray.size(); r++){
			JsonObject jsonCoordinateObj= coordinateArray.getJsonObject(r);
			double xval= getDataAsDouble(jsonCoordinateObj,"X");
			double yval= getDataAsDouble(jsonCoordinateObj, "Y");
		    }
		 
		    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBREGION_NAME, dataManager.getSubName())
		    .add(JSON_CAPITAL, dataManager.getCapital())
		    .add(JSON_LEADER, dataManager.getLeader())
		    .add(JSON_RED, dataManager.getRed())
		    .add(JSON_BLUE, dataManager.getBlue())
		    .add(JSON_GREEN, dataManager.getGreen())
		    .add(JSON_FLAG_IMAGE, dataManager.getFlagImage())
		    .add(JSON_LEADER_IMAGE, dataManager.getLeaderImage()).build();
		    arrayBuilder.add(itemJson);
		    }   
	
	JsonArray subitemsArray = arrayBuilder.build();//.
	}
	
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_REGION_NAME, mapName)
		.add(JSON_COORDINATE_PATH, mapFilePath)
		.add(JSON_BACKGROUND_COLOR, backgroundcolor)
		.add(JSON_BORDER_COLOR, bordercolor)
		.add(JSON_MAP_ZOOM, mapzoom)
		.add(JSON_MAP_WIDTH, mapwidth)
		.add(JSON_MAP_HEIGHT, mapheight)
		.add(JSON_BORDER_THICKNESS, borderThickness)
		
		.add(JSON_MAP_SEAL, sealpath)
		.add(JSON_MAP_FLAG,flagpath)
		
//		.add(JSON_SUBARRAY,)
		
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
	
      





 @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	
	
  	// CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
	dataManager.subregionPolygons.clear();
	dataManager.getImageItems().clear();
	dataManager.getCoordinates().clear();
	
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
	//not sure for now
	String sealpath = dataManager.getSealPath();
	String flagpath=dataManager.getFlagPath();
	
	//Map edit items
	String mapName = json.getString(JSON_REGION_NAME);
        String backgroundcolor= json.getString(JSON_BACKGROUND_COLOR);
	String bordercolor=json.getString(JSON_BORDER_COLOR);
	String mapFilePath=json.getString(JSON_COORDINATE_PATH);
	double borderThickness=json.getInt(JSON_BORDER_THICKNESS);
	double mapwidth =json.getInt(JSON_MAP_WIDTH);
	double mapheight=json.getInt(JSON_MAP_HEIGHT);
	double mapzoom=json.getInt(JSON_MAP_ZOOM);
	
	dataManager.setMapName(mapName);
	dataManager.setBackgroundColor(backgroundcolor);
	dataManager.setBorderColor(bordercolor);
	dataManager.setBorderThickness(borderThickness);
	dataManager.setMapWidth(mapwidth);
	dataManager.setMapHeight(mapheight);
	dataManager.setMapZoom(mapzoom);
	dataManager.setMapFilePath(mapFilePath);
	
	
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	JsonObject json = loadJSONFile(filePath);	
	JsonArray subArray1 = json.getJsonArray(JSON_NUMBER_OF_SUBREGIONS);
	for (int i = 0; i < subArray1.size(); i++) { 
	    
	
	// NOW BUILD THE JSON ARRAY FOR THE SubRegions
	
		JsonArray subRegionPolygons = json.getJsonArray(JSON_SUBREGION_POLYGONS);
	 	    
	 
		for(int v=0; v<subRegionPolygons.size(); v++){
		  
		    JsonArray coordinateArray= subRegionPolygons.getJsonArray(v);
		    for (int r=0; r<coordinateArray.size(); r++){
			JsonObject jsonCoordinateObj= coordinateArray.getJsonObject(r);
			double xval= getDataAsDouble(jsonCoordinateObj,"X");
			double yval= getDataAsDouble(jsonCoordinateObj, "Y");
		    }
		 
		    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBREGION_NAME, dataManager.getSubName())
		    .add(JSON_CAPITAL, dataManager.getCapital())
		    .add(JSON_LEADER, dataManager.getLeader())
		    .add(JSON_RED, dataManager.getRed())
		    .add(JSON_BLUE, dataManager.getBlue())
		    .add(JSON_GREEN, dataManager.getGreen())
		    .add(JSON_FLAG_IMAGE, dataManager.getFlagImage())
		    .add(JSON_LEADER_IMAGE, dataManager.getLeaderImage()).build();
		    arrayBuilder.add(itemJson);
		    }   
	
	JsonArray subitemsArray = arrayBuilder.build();//.
	}
	
	
	
	


	
	Workspace workspace= dataManager.getWorkspace();
//	workspace.test2.getChildren().clear();
	
	//workspace.getOwner().setText(owner);
	//workspace.getName().setText(name);
	// AND NOW LOAD ALL THE ITEMS
	JsonArray jsonItemArray = json.getJsonArray(JSON_SUBARRAY);

	//loadMap(data, mapFilePath);
    }

    
    
    public void loadCoordinates(AppDataComponent data, String filePath) throws IOException {
       // coordinates= new ArrayList();
	DataManager dataManager = (DataManager)data;
	coordinates.clear();
	subregionPolygons.clear();
	
	//dataManager.render();
	
	dataManager.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	//	polygon=dataManager.getShape();
	JsonObject json = loadJSONFile(filePath);	
	JsonArray jsonItemArray1 = json.getJsonArray(JSON_SUBREGIONS);
	for (int i = 0; i < jsonItemArray1.size(); i++) { 
	   subregionPolygons= new ArrayList<Polygon>();
	   
	   JsonObject jsonObject= jsonItemArray1.getJsonObject(i);
	   JsonArray jsonItemArray2= jsonObject.getJsonArray(JSON_SUBREGION_POLYGONS);
	   
	   for (int c=0; c<jsonItemArray2.size(); c++){
	    
	   JsonArray testArr= jsonItemArray2.getJsonArray(c);
	   polygon=new Polygon();
	   
	   for (int j=0; j< testArr.size(); j++){
	       
		JsonObject jsonItemObject2= testArr.getJsonObject(j);
		double x= getDataAsDouble(jsonItemObject2,"X");
		double y= getDataAsDouble(jsonItemObject2, "Y");
		double width= dataManager.getWidth();
		double height= dataManager.getHeight();
	    polygon.getPoints().add((x+180.0)*(width/360.0));
	    polygon.getPoints().add((90-y)*(height/180.0));
	    polygon.setFill(Color.YELLOWGREEN);
	    subregionPolygons.add(polygon);
	    }
	
	  coordinates.add(polygon);
	   }
	
	}
	test();
	dataManager.render();  
	
    }
public void test(){
      for(int k=0; k<subregionPolygons.size();k++){
	    System.out.println(subregionPolygons.get(k));
	  }
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }


      
    

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
