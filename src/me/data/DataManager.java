
package me.data;

import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.JsonArray;
import javax.json.JsonObject;
import saf.components.AppDataComponent;
import me.MapViewerApp;
//import static me.data.SubRegionItems.DEFAULT_BLUE;
//import static me.data.SubRegionItems.DEFAULT_CAPITAL;
//import static me.data.SubRegionItems.DEFAULT_FLAG_IMAGE;
//import static me.data.SubRegionItems.DEFAULT_GREEN;
//import static me.data.SubRegionItems.DEFAULT_LEADER;
//import static me.data.SubRegionItems.DEFAULT_LEADER_IMAGE;
//import static me.data.SubRegionItems.DEFAULT_NAME;
//import static me.data.SubRegionItems.DEFAULT_RED;
import me.gui.Workspace;
import me.file.FileManager;
import saf.AppTemplate;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    AppTemplate app;

    StringProperty mapFilePath;
    StringProperty backgroundColor;
    StringProperty borderColor;
    StringProperty mapName;
    DoubleProperty mapZoom;
    DoubleProperty borderThickness;
    DoubleProperty mapWidth;
    DoubleProperty mapHeight;
    
    //Array of user images	
    ObservableList <StringProperty> imageItems;
    ArrayList<Polygon> subregionPolygons;
    Polygon polygon;
    ArrayList<Polygon> coordinates; // add all the subregion coordinates into sub items
    
    
    //hardcoded imagepaths 
    StringProperty sealPath;
    StringProperty flagPath;
 
  StringProperty subname;
  StringProperty capital;
  StringProperty leader;
  StringProperty flagImage;
  StringProperty leaderImage;
  IntegerProperty red;
  IntegerProperty blue;
  IntegerProperty green;
    


      
	
	




////subregion stuff
//    public static final String DEFAULT_NAME = "?";
//    public static final String DEFAULT_CAPITAL = "?";
//    public static final String DEFAULT_LEADER = "?";
//    public static final String DEFAULT_LEADER_IMAGE = "?";
//    public static final String DEFAULT_FLAG_IMAGE = "?";
//    public static final int DEFAULT_RED = 0;
//    public static final int DEFAULT_BLUE = 0;
//    public static final int DEFAULT_GREEN = 0;
//	
	
  
    
    
    public DataManager(AppTemplate initApp) {
        app = initApp;
	polygon = new Polygon();
	imageItems= FXCollections.observableArrayList();
	coordinates= new ArrayList<Polygon>();
	subregionPolygons= new ArrayList<Polygon>();
	
	
	mapFilePath = new SimpleStringProperty("Default Map Path");
	backgroundColor = new SimpleStringProperty("Default Background Color");
	borderColor = new SimpleStringProperty("Default Border Color");
	mapName = new SimpleStringProperty("Default Map Name");
	mapZoom = new SimpleDoubleProperty(0.0);
	borderThickness =  new SimpleDoubleProperty(0.0);
	mapWidth = new SimpleDoubleProperty(0.0);
	mapHeight = new SimpleDoubleProperty(0.0);
	sealPath = new SimpleStringProperty("Default Seal Path");
	flagPath = new SimpleStringProperty("Default Flag Path");
	
	
	
	
	subname = new SimpleStringProperty("Default subregion name");
	capital = new SimpleStringProperty("Default capital");
	leader = new SimpleStringProperty("Default leader");
	flagImage= new SimpleStringProperty("Default flag image path");
	leaderImage= new SimpleStringProperty("Default leader image path");
	red = new SimpleIntegerProperty(0);
	blue=new SimpleIntegerProperty(0);
	green=new SimpleIntegerProperty(0);
    }

    public ArrayList<Polygon> getCoordinates() {
	return coordinates;
    }

  
    public ArrayList<Polygon> getSubregionPolygons() {
	return subregionPolygons;
    }

    public void setSubregionPolygons(ArrayList<Polygon> itemPolygons) {
	this.subregionPolygons = itemPolygons;
    }

 
     public String getLeaderImage() {
	return leaderImage.get();
    }
     
     public void setLeaderImage(String path){
	 leaderImage.set(path);
     }
     
      public String getFlagImage() {
	return flagImage.get();
    }
    
     public void setFlagImage(String path){
	 flagImage.set(path);
     }
    
    public String getSubName() {
	return subname.get();
    }
    
    public void setSubName(String input){
	subname.set(input);
    }

    public String getCapital() {
	return capital.get();
    }
    
    
    public void setCapital(String input){
	capital.set(input);
    }


    public String getLeader() {
	return leader.get();
    }
    
    
    public void setLeader(String input){
	leader.set(input);
    }

    public int getRed() {
	return red.get();
    }
    
    
    public void setRed(int input){
	red.set(input);
    }

    public int getBlue() {
	return blue.get();
    }
    
       public void setBlue(int input){
	blue.set(input);
    }
       
    public int getGreen() {
	return green.get();
    }
     
       public void setGreen(int input){
	green.set(input);
    }
    
       
       public Boolean haveCapitals(){
	   if (capital.get().equals("?")){
	       return false;
	   }
	   else return true;
       }
       
       
       public Boolean haveLeaders(){
	      if (leader.get().equals("?")){
	       return false;
	   }
	   else return true;
       }
       
       public Boolean haveFlags(){
	   if(flagImage.get().equals("?")){
	       return false;
	   }
	          
	   else return true;
       }

    
	

    
public double getWidth( ){
   
double value=app.getGUI().getWindow().getWidth();
return value;
    }
    
public double getHeight(){
	   
double value=app.getGUI().getWindow().getHeight();
return value;
    }
    
    public void render(){
	
	Workspace workspace = (Workspace) app.getWorkspaceComponent();
	
	
	for (int i=0; i <coordinates.size();i++){
	 workspace.test2.getChildren().add(coordinates.get(i));
	
	}	
    }
    
    @Override
    public void reset() {
        
	Workspace workspace = (Workspace) app.getWorkspaceComponent();
	
	//workspace.test2.getChildren().clear();
    }

    
        public Workspace getWorkspace(){
	Workspace workspace = (Workspace) app.getWorkspaceComponent();
	return workspace;
    }

    public Polygon getPolygon() {
	return polygon;
    }

    public void setPolygon(Polygon polygon) {
	this.polygon = polygon;
    }

    public String getMapFilePath() {
	return mapFilePath.get();
    }

    public void setMapFilePath(String input) {
	mapFilePath.set(input);
    }

    public String getBackgroundColor() {
	return backgroundColor.get();
    }

    public void setBackgroundColor(String input) {
	backgroundColor.set(input);
    }

    public String getBorderColor() {
	return borderColor.get();
    }

    public void setBorderColor(String input) {
	borderColor.set(input);
    }

    public String getMapName() {
	return mapName.get();
    }

    public void setMapName(String input) {
	mapName.set(input);
    }

    public double getMapZoom() {
	return mapZoom.get();
    }

    public void setMapZoom(double val) {
          mapZoom.set(val);
    }

    public double getBorderThickness() {
	return borderThickness.get();
    }

    public void setBorderThickness(double val) {
	borderThickness.set(val);
    }

    public double getMapWidth() {
	return mapWidth.get();
    }

    public void setMapWidth(double val) {
	mapWidth.set(val);
    }

    public double getMapHeight() {
	return mapHeight.get();
    }

    public void setMapHeight(double val) {
	   mapHeight.set(val);
    }
    
//    public ObservableList<SubRegionItems> getSubregionItems() {
//	return subItems;
//    }
//     
//    public void addSubItem(SubRegionItems element) {
//        subItems.add(element);
//    }
//    public void setSubItems(ObservableList <SubRegionItems> subItems){
//	this.subItems=subItems;
//    }
    
    public ObservableList<StringProperty> getImageItems() {
	return imageItems;
    }

    public void setImageItems(ObservableList<StringProperty> imageItems) {
	this.imageItems = imageItems;
    }

    public String getSealPath() {
	return sealPath.get();
    }

    public void setSealPath(String input) {
	sealPath.set(input);
    }

    public String getFlagPath() {
	return flagPath.get();
    }

    public void setFlagPath(String input) {
	flagPath.set(input);
    }
  
	
	
	
	
	
	
	
	
	
	
	
	
	
//    public void createAndorraMap() {
//	
//
//
//
//	backgroundColor.set("-fx-background-color:#dc6e01"); 
//	mapFilePath.set("./raw_map_data/Andorra.json");
//	borderColor.set("-fx-background-color:black");
//	mapName.set("Andorra");
//	
//	
//
//	
//	
//	sealPath.set("file:./export/The World/Europe/Andorra/Andorra Seal.png");
//	flagPath.set("file:./export/The World/Europe/Andorra Flag.png");
//	imageItems.addAll(flagPath, sealPath);
//
//	
//	
//	
//	for (int j=0; j<itemPolygons.size(); j++){
//	
//	setName("Ordino");
//	ordino.setCapital("Ordino (town)");
//	ordino.setLeader("Ventura Espot");
//	ordino.setRed(200);
//	ordino.setBlue(200);
//	ordino.setGreen(200);
//	ordino.setFlagImage("file:./export/The World/Europe/Andorra/Ordino Flag.png");
//	ordino.setLeaderImage("file:./export/The World/Europe/Andorra/Ventura Espot.png");
//	subs.add(ordino);
//
//	}
//	
//	carillo= new SubRegionItems();
//	carillo.setName("Carillo");
//	carillo.setCapital("Carillo (town)");
//	carillo.setRed(198);
//	carillo.setBlue(198);
//	carillo.setGreen(198);
//	carillo.setFlagImage("file:./export/The World/Europe/Andorra/Carillo Flag.png");
//	carillo.setLeader("Enric Casadevall Medrano");
//	carillo.setLeaderImage("file:./export/The World/Europe/Andorra/Enric Casadevall Medrano.png");
//	subs.add(carillo);
//	
	
//	subRegionItem3 = new SubRegionItem();
//	subRegionItem3.setSubRegionName("Encamp");
//	subRegionItem3.setCapital("Encamp (town)");
//	subRegionItem3.setColorRed(196);
//	subRegionItem3.setColorBlue(196);
//	subRegionItem3.setColorGreen(196);
//	subRegionItem3.setFlagPath(flagPath);
//	subRegionItem3.setLeader("Miquel Alís Font");
//	subRegionItem3.setLeaderPath(flagPath);
////	addSubRegionItems(subRegionItem3);
//	
//	
//	
//		
//	subRegionItem4 = new SubRegionItem();
//	subRegionItem4.setSubRegionName("Escaldes-Engordany");
//	subRegionItem4.setCapital("Escaldes-Engordany (town)");
//	subRegionItem4.setColorRed(194);
//	subRegionItem4.setColorBlue(194);
//	subRegionItem4.setColorGreen(194);
//	subRegionItem4.setFlagPath(flagPath);
//	subRegionItem4.setLeader("Montserrat Capdevila Pallarés");
//	subRegionItem4.setLeaderPath(flagPath);
////addSubRegionItems(subRegionItem4);
//
//       		
//	subRegionItem5 = new SubRegionItem();
//	subRegionItem5.setSubRegionName("La Massana");
//	subRegionItem5.setCapital("La Massana (town)");
//	subRegionItem5.setColorRed(192);
//	subRegionItem5.setColorBlue(192);
//	subRegionItem5.setColorGreen(192);
//	subRegionItem5.setFlagPath(flagPath);
//	subRegionItem5.setLeader("Josep Areny");
//	subRegionItem5.setLeaderPath(flagPath);
////	 subs.add(subRegionItem5);
//
//	 
//	 subRegionItem6 = new SubRegionItem();
//	subRegionItem6.setSubRegionName("Andorra la Vella");
//	subRegionItem6.setCapital("Andorra la Vella (city)");
//	subRegionItem6.setColorRed(190);
//	subRegionItem6.setColorBlue(190);
//	subRegionItem6.setColorGreen(190);
//	subRegionItem6.setFlagPath(flagPath);
//	subRegionItem6.setLeader("Maria Rosa Ferrer Obiols");
//	subRegionItem6.setLeaderPath(flagPath);
////	 addSubRegionItems(subRegionItem6);
//	 
//	 
//	subRegionItem7 = new SubRegionItem();
//	subRegionItem7.setSubRegionName("Sant Julia de Loria");
//	subRegionItem7.setCapital("Sant Julia de Loria (town)");
//	subRegionItem7.setColorRed(188);
//	subRegionItem7.setColorBlue(188);
//	subRegionItem7.setColorGreen(188);
//	subRegionItem7.setFlagPath(flagPath);
//	subRegionItem7.setLeader("Josep Pintat Forné");
//	subRegionItem7.setLeaderPath(flagPath);
////	 addSubRegionItems(subRegionItem7);
//	
//	
	
	
	
	
	
	
}
    
    
//        public void createSanMarinoMap(){
//
//
//	backgroundColor = "-fx-background-color:#dc6e01";
//	MapCoordinates="./raw_map_data/Andorra.json";
//	borderColor = "-fx-background-color:black";
//	
//	mapName = "Andorra";
//	
//	
//	
//	
//	
////	items.add(editMapItem);
//	
////	addEditItems(editMapItem);
//	
//	
//	
//	sealPath = "file:./export/The World/Europe/Andorra/Andorra Seal.png";
//	flagPath="file:./export/The World/Europe/Andorra Flag.png";
//	
////	
////	filePathItem = new FilePathItem();
////	filePathItem.setFilePathItem(sealPath);
////	addFilePath(filePathItem);
//	
////	
////	filePathItem2 = new FilePathItem();
////	filePathItem2.setFilePathItem(flagPath);
////	addFilePath(filePathItem2);
//	
//	
//
//	subRegionItem = new SubRegionItem();
//	subRegionItem.setSubRegionName("Ordino");
//	subRegionItem.setCapital("Ordino (town)");
//	subRegionItem.setColorRed(200);
//	subRegionItem.setColorBlue(200);
//	subRegionItem.setColorGreen(200);
//	subRegionItem.setFlagPath(flagPath);
//	subRegionItem.setLeader("Ventura Espot");
//	subRegionItem.setLeaderPath(flagPath);
////	addSubRegionItems(subRegionItem);
//
//	
//	
//	subRegionItem2 = new SubRegionItem();
//	subRegionItem2.setSubRegionName("Canillo");
//	subRegionItem2.setCapital("Canillo (town)");
//	subRegionItem2.setColorRed(198);
//	subRegionItem2.setColorBlue(198);
//	subRegionItem2.setColorGreen(198);
//	subRegionItem2.setFlagPath(flagPath);
//	subRegionItem2.setLeader("Enric Casadevall Medrano");
//	subRegionItem2.setLeaderPath(flagPath);
////	addSubRegionItems(subRegionItem2);
//	
//	
//	subRegionItem3 = new SubRegionItem();
//	subRegionItem3.setSubRegionName("Encamp");
//	subRegionItem3.setCapital("Encamp (town)");
//	subRegionItem3.setColorRed(196);
//	subRegionItem3.setColorBlue(196);
//	subRegionItem3.setColorGreen(196);
//	subRegionItem3.setFlagPath(flagPath);
//	subRegionItem3.setLeader("Miquel Alís Font");
//	subRegionItem3.setLeaderPath(flagPath);
////	addSubRegionItems(subRegionItem3);
//	
//	
//	
//		
//	subRegionItem4 = new SubRegionItem();
//	subRegionItem4.setSubRegionName("Escaldes-Engordany");
//	subRegionItem4.setCapital("Escaldes-Engordany (town)");
//	subRegionItem4.setColorRed(194);
//	subRegionItem4.setColorBlue(194);
//	subRegionItem4.setColorGreen(194);
//	subRegionItem4.setFlagPath(flagPath);
//	subRegionItem4.setLeader("Montserrat Capdevila Pallarés");
//	subRegionItem4.setLeaderPath(flagPath);
////addSubRegionItems(subRegionItem4);
//
//       		
//	subRegionItem5 = new SubRegionItem();
//	subRegionItem5.setSubRegionName("La Massana");
//	subRegionItem5.setCapital("La Massana (town)");
//	subRegionItem5.setColorRed(192);
//	subRegionItem5.setColorBlue(192);
//	subRegionItem5.setColorGreen(192);
//	subRegionItem5.setFlagPath(flagPath);
//	subRegionItem5.setLeader("Josep Areny");
//	subRegionItem5.setLeaderPath(flagPath);
////	 subs.add(subRegionItem5);
//
//	 
//	 subRegionItem6 = new SubRegionItem();
//	subRegionItem6.setSubRegionName("Andorra la Vella");
//	subRegionItem6.setCapital("Andorra la Vella (city)");
//	subRegionItem6.setColorRed(190);
//	subRegionItem6.setColorBlue(190);
//	subRegionItem6.setColorGreen(190);
//	subRegionItem6.setFlagPath(flagPath);
//	subRegionItem6.setLeader("Maria Rosa Ferrer Obiols");
//	subRegionItem6.setLeaderPath(flagPath);
////	 addSubRegionItems(subRegionItem6);
//	 
//	 
//	subRegionItem7 = new SubRegionItem();
//	subRegionItem7.setSubRegionName("Sant Julia de Loria");
//	subRegionItem7.setCapital("Sant Julia de Loria (town)");
//	subRegionItem7.setColorRed(188);
//	subRegionItem7.setColorBlue(188);
//	subRegionItem7.setColorGreen(188);
//	subRegionItem7.setFlagPath(flagPath);
//	subRegionItem7.setLeader("Josep Pintat Forné");
//	subRegionItem7.setLeaderPath(flagPath);
//
//
//
//
//
//
//
//
//
//
//
//
//	MapCoordinates = "file:./raw_map_data/San Marino.json";
//	
//}
//	
//	
//	
//	public void createSlovakiaMap(){
////	editMapItem= new EditMapItem();
////	editMapItem.setMapName("Slovakia");
////	editMapItem.setBackgroundColor("-fx-background-color:#dc6e01");
////	editMapItem.setBorderColor("-fx-background-color:black");
////	
////	Flag=new Image("file:./export/The World/Europe/Slovakia Flag.png");
////	Seal=new Image("file:./export/The World/Europe/Slovakia/Slovakia Seal.png");
////	Flag2= new ImageView(Flag);
////	Seal2=new ImageView(Seal);
//
//
//
//
//	backgroundColor = "-fx-background-color:#dc6e01";
//	MapCoordinates="./raw_map_data/Andorra.json";
//	borderColor = "-fx-background-color:black";
//	
//	mapName = "Andorra";
//	
//	
//	
//	
//	
////	items.add(editMapItem);
//	
////	addEditItems(editMapItem);
//	
//	
//	
//	sealPath = "file:./export/The World/Europe/Andorra/Andorra Seal.png";
//	flagPath="file:./export/The World/Europe/Andorra Flag.png";
//	
////	
////	filePathItem = new FilePathItem();
////	filePathItem.setFilePathItem(sealPath);
////	addFilePath(filePathItem);
//	
////	
////	filePathItem2 = new FilePathItem();
////	filePathItem2.setFilePathItem(flagPath);
////	addFilePath(filePathItem2);
//	
//	
//
//	subRegionItem = new SubRegionItem();
//	subRegionItem.setSubRegionName("Ordino");
//	subRegionItem.setCapital("Ordino (town)");
//	subRegionItem.setColorRed(200);
//	subRegionItem.setColorBlue(200);
//	subRegionItem.setColorGreen(200);
//	subRegionItem.setFlagPath(flagPath);
//	subRegionItem.setLeader("Ventura Espot");
//	subRegionItem.setLeaderPath(flagPath);
////	addSubRegionItems(subRegionItem);
//
//	
//	
//	subRegionItem2 = new SubRegionItem();
//	subRegionItem2.setSubRegionName("Canillo");
//	subRegionItem2.setCapital("Canillo (town)");
//	subRegionItem2.setColorRed(198);
//	subRegionItem2.setColorBlue(198);
//	subRegionItem2.setColorGreen(198);
//	subRegionItem2.setFlagPath(flagPath);
//	subRegionItem2.setLeader("Enric Casadevall Medrano");
//	subRegionItem2.setLeaderPath(flagPath);
////	addSubRegionItems(subRegionItem2);
//	
//	
//	subRegionItem3 = new SubRegionItem();
//	subRegionItem3.setSubRegionName("Encamp");
//	subRegionItem3.setCapital("Encamp (town)");
//	subRegionItem3.setColorRed(196);
//	subRegionItem3.setColorBlue(196);
//	subRegionItem3.setColorGreen(196);
//	subRegionItem3.setFlagPath(flagPath);
//	subRegionItem3.setLeader("Miquel Alís Font");
//	subRegionItem3.setLeaderPath(flagPath);
////	addSubRegionItems(subRegionItem3);
//	
//	
//	
//		
//	subRegionItem4 = new SubRegionItem();
//	subRegionItem4.setSubRegionName("Escaldes-Engordany");
//	subRegionItem4.setCapital("Escaldes-Engordany (town)");
//	subRegionItem4.setColorRed(194);
//	subRegionItem4.setColorBlue(194);
//	subRegionItem4.setColorGreen(194);
//	subRegionItem4.setFlagPath(flagPath);
//	subRegionItem4.setLeader("Montserrat Capdevila Pallarés");
//	subRegionItem4.setLeaderPath(flagPath);
////addSubRegionItems(subRegionItem4);
//
//       		
//	subRegionItem5 = new SubRegionItem();
//	subRegionItem5.setSubRegionName("La Massana");
//	subRegionItem5.setCapital("La Massana (town)");
//	subRegionItem5.setColorRed(192);
//	subRegionItem5.setColorBlue(192);
//	subRegionItem5.setColorGreen(192);
//	subRegionItem5.setFlagPath(flagPath);
//	subRegionItem5.setLeader("Josep Areny");
//	subRegionItem5.setLeaderPath(flagPath);
////	 subs.add(subRegionItem5);
//
//	 
//	 subRegionItem6 = new SubRegionItem();
//	subRegionItem6.setSubRegionName("Andorra la Vella");
//	subRegionItem6.setCapital("Andorra la Vella (city)");
//	subRegionItem6.setColorRed(190);
//	subRegionItem6.setColorBlue(190);
//	subRegionItem6.setColorGreen(190);
//	subRegionItem6.setFlagPath(flagPath);
//	subRegionItem6.setLeader("Maria Rosa Ferrer Obiols");
//	subRegionItem6.setLeaderPath(flagPath);
////	 addSubRegionItems(subRegionItem6);
//	 
//	 
//	subRegionItem7 = new SubRegionItem();
//	subRegionItem7.setSubRegionName("Sant Julia de Loria");
//	subRegionItem7.setCapital("Sant Julia de Loria (town)");
//	subRegionItem7.setColorRed(188);
//	subRegionItem7.setColorBlue(188);
//	subRegionItem7.setColorGreen(188);
//	subRegionItem7.setFlagPath(flagPath);
//	subRegionItem7.setLeader("Josep Pintat Forné");
//	subRegionItem7.setLeaderPath(flagPath);
//	MapCoordinates = "file:./raw_map_data/Slovakia.json";
////	
//}

//}
