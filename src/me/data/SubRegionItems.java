///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package me.data;
//
//import java.util.ArrayList;
//import javafx.beans.property.IntegerProperty;
//import javafx.beans.property.SimpleIntegerProperty;
//import javafx.beans.property.SimpleStringProperty;
//import javafx.beans.property.StringProperty;
//import javafx.collections.ObservableList;
//import javafx.scene.shape.Polygon;
//
///**
// *
// * @author Tchou808
// */
//public class SubRegionItems {
//    
//    public static final String DEFAULT_NAME = "?";
//    public static final String DEFAULT_CAPITAL = "?";
//    public static final String DEFAULT_LEADER = "?";
//    public static final String DEFAULT_LEADER_IMAGE = "?";
//    public static final String DEFAULT_FLAG_IMAGE = "?";
//    public static final int DEFAULT_RED = 0;
//    public static final int DEFAULT_BLUE = 0;
//    public static final int DEFAULT_GREEN = 0;
//	
//    final StringProperty name;
//    final StringProperty capital;
//    final StringProperty leader;
//    final StringProperty flagImage;
//    final StringProperty leaderImage;
//    final IntegerProperty red;
//    final IntegerProperty blue;
//    final IntegerProperty green;
//    public ArrayList<Polygon> subregionPolygons;
//    
//    
//    public SubRegionItems(){
//	name = new SimpleStringProperty(DEFAULT_NAME);
//	capital = new SimpleStringProperty(DEFAULT_CAPITAL);
//	leader = new SimpleStringProperty(DEFAULT_LEADER);
//	flagImage= new SimpleStringProperty(DEFAULT_FLAG_IMAGE);
//	leaderImage= new SimpleStringProperty(DEFAULT_LEADER_IMAGE);
//	red = new SimpleIntegerProperty(DEFAULT_RED);
//	blue=new SimpleIntegerProperty(DEFAULT_BLUE);
//	green=new SimpleIntegerProperty(DEFAULT_GREEN);
//
//    }
//
//    public SubRegionItems(String initname, String initcapital, String initleader, int initred, int initblue, int initgreen) {
//	this();
//	name.set(initname);
//	capital.set(initcapital);
//	leader.set(initleader);
//	red.set(initred);
//	blue.set(initblue);
//	green.set(initgreen);
//    }
//
//    public ArrayList<Polygon> getSubregionPolygons() {
//	return subregionPolygons;
//    }
//
//    public void setSubregionPolygons(ArrayList<Polygon> itemPolygons) {
//	this.subregionPolygons = itemPolygons;
//    }
//
// 
//     public String getLeaderImage() {
//	return leaderImage.get();
//    }
//     
//     public void setLeaderImage(String path){
//	 leaderImage.set(path);
//     }
//     
//      public String getFlagImage() {
//	return flagImage.get();
//    }
//    
//     public void setFlagImage(String path){
//	 flagImage.set(path);
//     }
//    
//    public String getName() {
//	return name.get();
//    }
//    
//    public void setName(String input){
//	name.set(input);
//    }
//
//    public String getCapital() {
//	return capital.get();
//    }
//    
//    
//    public void setCapital(String input){
//	capital.set(input);
//    }
//
//
//    public String getLeader() {
//	return leader.get();
//    }
//    
//    
//    public void setLeader(String input){
//	leader.set(input);
//    }
//
//    public int getRed() {
//	return red.get();
//    }
//    
//    
//    public void setRed(int input){
//	red.set(input);
//    }
//
//    public int getBlue() {
//	return blue.get();
//    }
//    
//       public void setBlue(int input){
//	blue.set(input);
//    }
//       
//    public int getGreen() {
//	return green.get();
//    }
//     
//       public void setGreen(int input){
//	green.set(input);
//    }
//    
//       
//       public Boolean haveCapitals(){
//	   if (capital.get().equals("?")){
//	       return false;
//	   }
//	   else return true;
//       }
//       
//       
//       public Boolean haveLeaders(){
//	      if (leader.get().equals("?")){
//	       return false;
//	   }
//	   else return true;
//       }
//       
//       public Boolean haveFlags(){
//	   if(flagImage.get().equals("?")){
//	       return false;
//	   }
//	          
//	   else return true;
//       }
//}
