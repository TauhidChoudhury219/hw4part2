/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me;

/**
 *
 * @author McKillaGorilla
 */
public enum PropertyType {
    OK_PROMPT,
    CANCEL_PROMPT,
   
   MAP,
    CREATE_NEWMAP,
    EXPORT_MAP,
    
    CHANGE_MAP_NAME,
    ADD_IMAGE,
    REMOVE_IMAGE,
   CHANGE_MAP_COLOR,
   CHANGE_MAP_BORDERCOLOR,
   CHANGE_MAP_BORDERTHICK,
    NEW_MAP,
    RANDOM_COLORS,
    
    
    CHANGE_MAP_NAME_ICON,
    ADD_IMAGE_ICON,
    REMOVE_IMAGE_ICON,
   CHANGE_MAP_COLOR_ICON,
   CHANGE_MAP_BORDERCOLOR_ICON,
   CHANGE_MAP_BORDERTHICK_ICON,
    NEW_MAP_ICON,
    RANDOM_COLORS_ICON,
    EXPORT_MAP_ICON,
    WORKSPACE_HEADING_LABEL,
    
    
    // New Map Dialog Labels
    HELLO_LABEL,
    REGION_NAME_LABEL,
    PARENT_REGION_LABEL,
    DATA_FILE_LABEL,
    
    //Change Dimensions
    WIDTH_LABEL,
    HEIGHT_LABEL,
    
    //Edit Subregion 
    CAPITAL_LABEL,
    LEADER_NAME_LABEL,
    FLAG_IMAGE_LABEL,
    LEADER_IMAGE_LABEL,
    
    
 
}
