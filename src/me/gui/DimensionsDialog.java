/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gui;

/**
 *
 * @author Tchou808
 */




/**
 *
 * @author Tchou808
 */

import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import me.PropertyType;
import properties_manager.PropertiesManager;


/**
 *
 * @author Tchou808
 */
public class DimensionsDialog {
    
    Stage place;
    
    VBox layout1;
    
    
   
    HBox WidthPane;   
    TextField  widthText;
    
    HBox HeightPane;
    TextField heightText;
    
    
  
    Button ok;
    Button cancel;
    HBox buttonPane;
 
    VBox layout2;
    HBox whole;
    VBox complete;
    public String selection="X";
   
 
     Label  Width;
	     Label Height;
    
    public DimensionsDialog(){
	   place = new Stage(); 
	   place.initModality(Modality.APPLICATION_MODAL);
	   
	
	   layout2=new VBox();
	   whole=new HBox();
	   complete=new VBox();
	   layout1= new VBox();
	   
	   
   PropertiesManager props = PropertiesManager.getPropertiesManager();
	   
	 
	   Label HELLO_LABEL= new Label();
	   HELLO_LABEL.setText(props.getProperty(PropertyType.HELLO_LABEL));
	   
	//   hello.setFont(Font.font(25));   css sheet
	   
	  
	WidthPane= new HBox();
	   
	Width = new Label("Enter Width");
	    
	   widthText= new TextField();
	//   categoryPane.getChildren().addAll(categoryLabel, categoryText);
	   
	   
	   HeightPane= new HBox();
	  
	Height = new Label ("Enter Height");
	   
	   heightText= new TextField();
	//   descriptionPane.getChildren().addAll(descriptionLabel,descriptionText);
	   
	
	  
	  
	  
	  
	  // startText.setMaxWidth(150);
	  // StartDateLabel.setMinWidth(50);
	  // startDatePane.getChildren().addAll(StartDateLabel, startText);
	   
	
	   
	   ok= new Button("Ok");
	   cancel= new Button("Cancel");
	   buttonPane= new HBox(ok, cancel);
	   
	
		
		
		layout1.getChildren().addAll(Width, Height);
		layout2.getChildren().addAll(widthText, heightText);
		layout1.setMinWidth(150);
		layout1.setSpacing(50);
		layout2.setSpacing(40);
		whole.getChildren().addAll(layout1, layout2);
		
		whole.setSpacing(50);
		complete.getChildren().addAll(HELLO_LABEL, whole, buttonPane);
		complete.setSpacing(50);
		complete.setPadding(new Insets(100, 80, 100, 80));
		
		
	   Scene dialogScene = new Scene(complete);
	   place.setScene(dialogScene);
    }
   
}

