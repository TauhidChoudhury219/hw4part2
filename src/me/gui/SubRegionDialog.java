/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import me.PropertyType;
import properties_manager.PropertiesManager;

/**
 *
 * @author Tchou808
 */
public class SubRegionDialog {
    Label NameLabel;
	     TextField NameText;
        Stage place;
    
    VBox layout1;
    
    
   Label CapitalLabel;
	TextField CapitalText;
    HBox WidthPane;   
    TextField  widthText;
    
    HBox HeightPane;
    TextField heightText;
    
    
	 TextField FlagText;  
	TextField LeaderText;
	Label LeaderLabel;
	Label FlagLabel;	
	Label LeaderImageLabel;	
	TextField LeaderImageText;
  
    Button ok;
    Button cancel;
    HBox buttonPane;
 
    VBox layout2;
    HBox whole;
    VBox complete;
    public String selection="X";
   
 
     Label  Width;
	     Label Height;
    
    public SubRegionDialog(){
	   place = new Stage(); 
	   place.initModality(Modality.APPLICATION_MODAL);
	   
	
	   layout2=new VBox();
	   whole=new HBox();
	   complete=new VBox();
	   layout1= new VBox();
	   
	   
   PropertiesManager props = PropertiesManager.getPropertiesManager();
	   
	 
	   Label HELLO_LABEL= new Label();
	   HELLO_LABEL.setText(props.getProperty(PropertyType.HELLO_LABEL));
	   
	//   hello.setFont(Font.font(25));   css sheet
	    
	    NameLabel = new Label("Name");
	    NameText = new TextField();
	   
	
	  
	
	CapitalLabel = new Label ("Capital");
	   
	
	CapitalText = new TextField();
	
	LeaderLabel = new Label ("Leader");
	LeaderText = new TextField();
	
	
	FlagLabel = new Label("Flag image");
	FlagText = new TextField();
		
		
	
	
	
	LeaderImageLabel = new Label("Leader image");
	
	LeaderImageText = new TextField();
	  
	 
	
	   
	   ok= new Button("Ok");
	   cancel= new Button("Cancel");
	   buttonPane= new HBox(ok, cancel);
	   
	
		
		
		layout1.getChildren().addAll(NameLabel, CapitalLabel, LeaderLabel, FlagLabel, LeaderImageLabel);
		layout2.getChildren().addAll(NameText, NameText,NameText,NameText,NameText);
		layout1.setMinWidth(150);
		layout1.setSpacing(50);
		layout2.setSpacing(40);
		whole.getChildren().addAll(layout1, layout2);
		
		whole.setSpacing(50);
		complete.getChildren().addAll(HELLO_LABEL, whole, buttonPane);
		complete.setSpacing(50);
		complete.setPadding(new Insets(100, 80, 100, 80));
		
		System.out.println("shihd");
	   Scene dialogScene = new Scene(complete);
	   place.setScene(dialogScene);
    }
    
}
