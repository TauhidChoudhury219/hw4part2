///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package me.gui;
//
//
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.control.cell.PropertyValueFactory;
//import javafx.application.Application;
//import javafx.geometry.Insets;
//import javafx.scene.Scene;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.layout.StackPane;
//import javafx.stage.Stage;
//import saf.ui.AppYesNoCancelDialogSingleton;
//import saf.ui.AppMessageDialogSingleton;
//import properties_manager.PropertiesManager;
//import saf.ui.AppGUI;
//import saf.AppTemplate;
//import saf.components.AppWorkspaceComponent;
//import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
//import static saf.settings.AppStartupConstants.PATH_IMAGES;
//import javafx.scene.control.Button;
//import javafx.scene.control.ColorPicker;
//import javafx.scene.control.ScrollPane;
//import javafx.scene.control.Slider;
//import javafx.scene.control.SplitPane;
//import javafx.scene.image.ImageView;
//import javafx.scene.layout.FlowPane;
//import javafx.scene.layout.HBox;
//import javafx.scene.layout.Pane;
//import javafx.scene.layout.StackPane;
//import javafx.scene.layout.VBox;
//import javafx.scene.paint.Color;
//import saf.components.AppWorkspaceComponent;
//import me.MapViewerApp;
//import me.PropertyType;
//import me.controller.MapEditorController;
//
//import me.data.DataManager;
//import properties_manager.PropertiesManager;
//import saf.ui.AppGUI;
//
//
///**
// *
// * @author McKillaGorilla
// */
//public class Workspace extends AppWorkspaceComponent {
//    MapViewerApp app;
//    public SplitPane center;
//    public Image map;
//    public ImageView img;
//    public StackPane sp;
//    AppGUI gui;
//    public final StackPane leftSide;
//    public  final StackPane rightSide;
//    public StackPane first;
//    public TableView mapTable;
//    public StackPane mapBox;
//    public TableColumn capital;
//    public TableColumn leader;
//    public TableColumn subRegionName;
//    public StackPane mapBox2;
//    
//    
//    public Workspace(MapViewerApp initApp) {
//        app = initApp;
//	mapTable=new TableView();
//        workspace = new Pane();
//	first = new StackPane();
//	center= new SplitPane();
//	leftSide= new StackPane();
//	rightSide= new StackPane();
//	//map= new Image("file:./images/france.png");
//	img= new ImageView(map);
//	mapBox=new StackPane();
//	mapBox2=new StackPane();
//	subRegionName = new TableColumn("SubRegion name");
//	capital = new TableColumn("Capital");
//	leader = new TableColumn("Leader");
//	
//	// KEEP THIS FOR LATER
//	app = initApp;
//
//	// KEEP THE GUI FOR LATER
//	 gui = app.getGUI();
//
//        // INIT ALL WORKSPACE COMPONENTS
//	layoutGUI();
//        
//        // AND SETUP EVENT HANDLING
//	setUpHandlers();
//    }
//    
//    public void layoutGUI(){
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//      
//
//    
//	mapTable.getColumns().addAll(subRegionName, capital, leader);
//
//
//	//mapBox.getChildren().add(img);
//	
//	mapBox.setPrefSize(802, 536);
////
////	mapBox.setMinHeight(536);	
//
////	mapBox2.getChildren().add(mapBox);
//
//	center.setMinHeight(app.getGUI().getAppPane().getHeight());
//	center.setMinWidth(app.getGUI().getAppPane().getWidth());
//
//	first.getChildren().add(mapTable);
//	first.setPadding(new Insets(100, 80, 100, 80));
//	
// //leftSide.getChildren().add(mapBox);
// rightSide.getChildren().addAll(first);
//
// center.getItems().addAll(leftSide, rightSide);
// center.setDividerPositions(0.5f, 0.9f);
// 
//	 
//	
//workspace.getChildren().add(mapBox);
//
//
//	reloadWorkspace();
//    }
//    
//     private void setUpHandlers() {
//
//	 
//	 
//	 
//	 
//     }
//    
//    @Override
//    public void reloadWorkspace() {
//        DataManager dataManager = (DataManager)app.getDataComponent();
//    }
//
//    @Override
//    public void initStyle() {
//        
//    }
//    
//    public StackPane getMapBox(){
//	return mapBox;
//    }
//}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gui;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import saf.components.AppWorkspaceComponent;

import java.awt.MouseInfo;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import me.MapViewerApp;

import me.data.DataManager;
import me.file.FileManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import saf.ui.AppGUI;
/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    public HBox testPane;
    public Pane test2;
    public SplitPane center;
    public StackPane rightSide;
    public StackPane leftSide;
    
    public StackPane first;
    public TableView mapTable;
    public StackPane mapBox;
    public TableColumn capital;
    public TableColumn leader;
    public TableColumn subRegionName;
   
   
   
   public Workspace(MapViewerApp initApp) {
   
	
	
        app = initApp;
        workspace = new Pane();

	testPane = new HBox();
	test2= new Pane();
	center= new SplitPane();
	rightSide=new StackPane();
	leftSide=new StackPane();
	mapTable=new TableView();
	subRegionName = new TableColumn("SubRegion name");
	capital = new TableColumn("Capital");
	leader = new TableColumn("Leader");
	first=new StackPane();
	
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	AppGUI gui = app.getGUI();

        // INIT ALL WORKSPACE COMPONENTS
	layoutGUI();
        
        // AND SETUP EVENT HANDLING
	setUpHandlers();
   }
   
  
   
   
   public void layoutGUI(){
   	
	
//	test2.setMinHeight(6000);
//	test2.setMinWidth(6000);
	//testPane.setAlignment(Pos.CENTER);
//	test2.setMinHeight(app.getGUI().getAppPane().getHeight());
//	test2.setMinWidth(app.getGUI().getAppPane().getWidth());
//	test2.getChildren().add(testPane);
	//test2.setPrefSize(802, 536);
	
	mapTable.getColumns().addAll(subRegionName, capital, leader);
		
	first.getChildren().add(mapTable);
	first.setPadding(new Insets(100, 80, 100, 80));
	rightSide.getChildren().addAll(first);
	
	
	leftSide.getChildren().add(test2);
	center.getItems().addAll(leftSide,rightSide);
	center.setDividerPositions(0.5f);
	center.setMinHeight(app.getGUI().getAppPane().getHeight());
	center.setMinWidth(app.getGUI().getAppPane().getWidth());
	workspace.getChildren().add(center);


	

	reloadWorkspace();
    }
   

   
   public void setUpHandlers(){
       	 
   }
    @Override
    public void reloadWorkspace() {
	DataManager dataManager = (DataManager)app.getDataComponent();
	
	
    }

    @Override
    public void initStyle() {
        
    }
     
     }
    






package me.gui;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import saf.components.AppWorkspaceComponent;

import java.awt.MouseInfo;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import me.MapViewerApp;

import me.data.DataManager;
import me.file.FileManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import saf.ui.AppGUI;
/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    public HBox testPane;
    public Pane test2;
    public SplitPane center;
    public StackPane rightSide;
    public StackPane leftSide;
    
    public StackPane first;
    public TableView mapTable;
    public StackPane mapBox;
    public TableColumn capital;
    public TableColumn leader;
    public TableColumn subRegionName;
   
   
   
   public Workspace(MapViewerApp initApp) {
   
	
	
        app = initApp;
        workspace = new Pane();

	testPane = new HBox();
	test2= new Pane();
	center= new SplitPane();
	rightSide=new StackPane();
	leftSide=new StackPane();
	mapTable=new TableView();
	subRegionName = new TableColumn("SubRegion name");
	capital = new TableColumn("Capital");
	leader = new TableColumn("Leader");
	first=new StackPane();
	
	 KEEP THIS FOR LATER
	app = initApp;

	 KEEP THE GUI FOR LATER
	AppGUI gui = app.getGUI();

         INIT ALL WORKSPACE COMPONENTS
	layoutGUI();
        
         AND SETUP EVENT HANDLING
	setUpHandlers();
   }
   
  
   
   
   public void layoutGUI(){
   	
	
	test2.setMinHeight(6000);
	test2.setMinWidth(6000);
	testPane.setAlignment(Pos.CENTER);
	test2.setMinHeight(app.getGUI().getAppPane().getHeight());
	test2.setMinWidth(app.getGUI().getAppPane().getWidth());
	test2.getChildren().add(testPane);
	test2.setPrefSize(802, 536);
	
	mapTable.getColumns().addAll(subRegionName, capital, leader);
		
	first.getChildren().add(mapTable);
	first.setPadding(new Insets(100, 80, 100, 80));
	rightSide.getChildren().addAll(first);
	
	
	leftSide.getChildren().add(test2);
	center.getItems().addAll(leftSide,rightSide);
	center.setDividerPositions(0.5f);
	center.setMinHeight(app.getGUI().getAppPane().getHeight());
	center.setMinWidth(app.getGUI().getAppPane().getWidth());
	workspace.getChildren().add(center);


	

	reloadWorkspace();
    }
   

   
   public void setUpHandlers(){
       	 
   }
    @Override
    public void reloadWorkspace() {
	DataManager dataManager = (DataManager)app.getDataComponent();
	
	
    }

    @Override
    public void initStyle() {
        
    }
     
     }
    

